Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete  # should autocomplete to first character, not full path
Set-PSReadLineOption -PredictionSource History
Set-PSReadLineOption -AddToHistoryHandler {
    param([string] $inp)
    if ("l" -eq $inp || "ll" -eq $inp || ".." -eq $inp) {
        $false
    } else {
        $true
    }
}

# OS specific scripts

$osPath = if ($IsWindows) { 'windows.ps1' } else {
    'linux.ps1'
}
$scriptPath = Join-Path "$PSScriptRoot" "$osPath"
if (Test-Path $scriptPath) {
    . "$scriptPath"
}

Remove-Variable scriptPath
Remove-Variable osPath


if (Test-Path "$PSScriptRoot\local.ps1") {
    . "$PSScriptRoot\local.ps1"
}


#Measure-Command {Write-Host "ti"; Import-Module Terminal-Icons}
# Import-Module Terminal-Icons


#. (Join-Path $PSScriptRoot  'tools' '.\just.completion.ps1')
if ($IsWindows) {
    #Measure-Command {Write-Host "omp"; oh-my-posh init pwsh --config "$env:POSH_THEMES_PATH\powerline.omp.json" | Invoke-Expression}
    (Measure-Command {
      #Write-Host "omp"
      Write-Host "sta"
      #$ENV:STARSHIP_CONFIG = 'C:\Users\tzy1\dotfiles\nix\starship\starship.toml'
      #$ENV:STARSHIP_CONFIG = 'E:\nosave\Projects\nix\dotfiles\nix\starship\starship.toml'
      #$ENV:POSH_THEMES_PATH = $PSScriptRoot
      Invoke-Expression (&starship init powershell)
      #oh-my-posh init pwsh --config "$env:POSH_THEMES_PATH\powerline.omp.json" | Invoke-Expression
      #. 'C:\Users\tzy1\Documents\PowerShell\ignored_modules\powershell-old\omp3.ps1'
      # & ([ScriptBlock]::Create((oh-my-posh init pwsh --config "$env:POSH_THEMES_PATH\powerline.omp.json" --print) -join "`n"))
      #& ([ScriptBlock]::Create((starship init powershell) -join "`n"))
      #. (Join-Path $PSScriptRoot '.\omp.ps1')

    }).TotalMilliseconds
    . (Join-Path $PSScriptRoot '.\aliases.ps1')

    #New-Alias -Name 'Set-PoshContext' -Value 'Set-EnvVar' -Scope Global -Force
}

#. (Join-Path $PSScriptRoot 'tools' '.\zoxide.init.ps1')

#Measure-Command {Write-Host "z"; Invoke-Expression (& { $hook = if ($PSVersionTable.PSVersion.Major -ge 6) { 'pwd' } else { 'prompt' } (zoxide init powershell --hook $hook | Out-String) })}
#Invoke-Expression (& { $hook = if ($PSVersionTable.PSVersion.Major -ge 6) { 'pwd' } else { 'prompt' } (zoxide init powershell --hook $hook | Out-String) })

Invoke-Expression (& { (zoxide init powershell | Out-String) })

# make sure that current oh-my-posh theme has 'osc99:true' in its config for windows and use 'duplicatePane/Tab' liberally
#Measure-Command {Write-Host "starship"; Invoke-Expression (&starship init powershell)}
if ($IsLinux) {
    if ($null -eq $env:TMUX) {
        $env:TERM = 'xterm-256color'
    } else {
        $env:TERM = 'tmux-256color'
    }
}

function Get-FullPath {
    [CmdletBinding()]
    [OutputType([String])]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        $Path
    )
    process {
        if (Test-Path $Path) {
            (Get-Item $Path).FullName
        } else {
            ""
        }
    }
}

Set-Alias -Name realpath -Value Get-FullPath

function Get-FolderSize {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        $Path
    )
    process {
        if ( (Test-Path $Path) -and (Get-Item $Path).PSIsContainer ) {
            $Measure = Get-ChildItem $Path -Recurse -Force -ErrorAction SilentlyContinue | Measure-Object -Property Length -Sum
            $SumK = '{0:N2}' -f ($Measure.Sum / 1Kb)
            $SumM = '{0:N2}' -f ($Measure.Sum / 1Mb)
            $SumG = '{0:N2}' -f ($Measure.Sum / 1Gb)
            [PSCustomObject]@{
                "Path" = $Path
                "Size(Kb)" = $SumK
                "Size(Mb)" = $SumM
                "Size(Gb)" = $SumG
            }
        }
    }
}

function New-SublimeProject {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $false)]
        $ProjectName
    )
    $fileName = "$ProjectName.sublime-project"
    if (Test-Path -Path $fileName) {
        Write-Output "Project file already exists, doing nothing"
    } else {
        $defaultProject = [PSCustomObject]@{
            folders = @(
                [PSCustomObject]@{
                    path = '.'
                }
            )
        }

        Set-Content -Path $fileName -Value (ConvertTo-Json $defaultProject)
    }
}
