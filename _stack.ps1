using namespace System.Management.Automation
using namespace System.Management.Automation.Language
Register-ArgumentCompleter -Native -CommandName 'stack' -ScriptBlock {
    param($wordToComplete, $commandAst, $cursorPosition)
    [string[]]$localCommand = @('--bash-completion-enriched')
    $localCommand += '--bash-completion-option-desc-length'
    $localCommand += '80'
    $localCommand += '--bash-completion-command-desc-length'
    $localCommand += '80'
    
    $hay = [System.Collections.Generic.List[string]]::new()
    foreach ($item in $commandAst.CommandElements) {
        $localCommand += '--bash-completion-word'
        $localCommand += "$item"
        $hay.Add($item.ToString())
    }


    $localCommand += '--bash-completion-index'
    if ($wordToComplete.Equals("")) {
        $localCommand += $commandAst.CommandElements.Count
    }
    else {
        $localCommand +=  $hay.IndexOf($wordToComplete)
    }

    $fp = if ($IsWindows) { 'E:\nosave\Projects\Haskell\ghcup\ghcup\bin\stack.exe' } else { '/data/homes/tzakrevs/.nix-profile/bin/stack' }
    [string[]]$inp = (& $fp @localCommand)

    [CompletionResult[]]$out = @()
    [string]$suffix = if ($inp.Count -eq 1) { ' ' } else { "" }
    foreach ($item in $inp) {
        $spl = $item.Split("`t")
        $show = $spl[0]
        $tooltip = if ($spl.Length -eq 1) { $spl[0] } else { $spl[1] }
        $crt = if ($show.StartsWith('-')) { [CompletionResultType]::ParameterName } else { [CompletionResultType]::ParameterValue }
        $out += [CompletionResult]::new($show + $suffix, $show, $crt, $tooltip)
    }
    $out
    # $out | ForEach-Object {$_}
}
