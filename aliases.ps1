Set-Alias b bat
Set-Alias -Name vi -Value vim
Set-Alias -Name v -Value vim

# hack to pass parameters
Function Personal-Up { Set-Location -Path .. }
Set-Alias -Name ".." -Value Personal-Up

Function Personal-Up2 { Set-Location -Path ..\.. }
Set-Alias -Name "..." -Value Personal-Up2
