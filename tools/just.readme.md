# Just
## Generate script

```
PS> just --completions powershell >> just.completion.ps1
```

Current `just` version:
```
PS> just --version
just 0.11.2
```