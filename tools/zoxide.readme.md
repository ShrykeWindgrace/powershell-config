# Zoxide tool

## Generate script

```powershell
$hook = if ($PSVersionTable.PSVersion.Major -lt 6) { 'prompt' } else { 'pwd' }
zoxide init --hook $hook powershell >> zoxide.init.ps1
```

Current zoxide version:
```
PS> zoxide --version
zoxide v0.8.0
```