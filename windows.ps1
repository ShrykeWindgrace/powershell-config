# windows

# encoding fix for terminal
$OutputEncoding = [console]::InputEncoding = [console]::OutputEncoding = New-Object System.Text.UTF8Encoding

$env:GIT_SSH = 'C:\Windows\System32\OpenSSH\ssh.exe'

Set-PSReadLineKeyHandler -Key Ctrl+d -Function DeleteCharOrExit #  as if we were in a linux env
Set-PSReadLineKeyHandler -Key Ctrl+k -Function DeleteToEnd

# lsd on my windows machines are slow because of domain handling, see issues in the lsd repo
# so we use regular ls
Set-Alias -Name l -Value ls

# On linux we would use UnixCompleters to shave a bit off startup time
#Measure-Command {Write-Host "posh-git"; Import-Module posh-git}
#Measure-Command {Write-Host "posh-glitter"; Import-Module posh-glitter}
Import-Module posh-glitter

function Show-FilePath ($File)
{
    $item = Get-Item $File
    $nodes = $item.FullName.Split([System.IO.Path]::DirectorySeparatorChar)
    $offset = 0
    foreach ($node in $nodes | Select-Object -SkipLast 1) {
        Write-Host ((" "* $offset * 2) +"└─📁" + $node)
        $offset ++
    }
    Write-Host ((" "* $offset * 2) +"└─✯" + $nodes[-1])
}

$env:RIPGREP_CONFIG_PATH = Join-Path (Split-Path $PROFILE.CurrentUserCurrentHost) '.ripgreprc'

function Update-Scoop ()
{
    Invoke-Expression -Command 'scoop update *'
}

function Write-Cow ()
{
    cowsay "You are not in vim anymore"
}

Set-Alias -Name ":q" -Value Write-Cow

$stackPath = Join-Path "$PSScriptRoot" "_stack.ps1"
if (Test-Path $stackPath) {
    . "$stackPath"
} else {
    Write-Host "Failed to find stack completion script at '$stackPath'"
}
Remove-Variable stackPath

function Use-CustomAliasExa () {
    Invoke-Expression -Command "eza -lab --git --icons --group-directories-first $args"
}
Set-Alias -Name ll -Value Use-CustomAliasExa
$env:EXA_COLORS='da=93' # dates are yellow, see https://github.com/eza-community/eza/blob/main/man/eza_colors.5.md
# todo: check bright colors once we get eza >= 0.11.1 (should be 93, bright yellow)

#Invoke-Expression "$(direnv hook pwsh)"  # requires a master build or a next release; it does not work in windows environment, only in its linux lookalikes

# copied from https://github.com/Moeologist/scoop-completion
#Measure-Command {
#    Write-Host "scoop-completion"
    # 60 ms, I can live with that
    Import-Module "$($(Get-Item $(Get-Command scoop.ps1).Path).Directory.Parent.FullName)\modules\scoop-completion"
#}

function Reset-Cursor () {
    Write-Host "`e[0 q"
}

# fix colors in a combo 'bat | less'
# See discussion here: https://github.com/gwsw/less/issues/538
$env:BAT_PAGER="less -RF -Da"
